//
//  ViewController.swift
//  G69L3
//
//  Created by Ivan Vasilevich on 3/4/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		let name = "Ivan"
		var a = 4.91
		let cokainc = 11
		print("c =", cokainc)
		a = 5
		print("a =", a)
		
//		let sum = a.rounded() + Double(c)
		
		let candyNumber = Int.random(in: 1...4)
		
		if candyNumber == 1 {
			print("Brosayu rabotu")
		}
		else if candyNumber == 2 {
			print("Brosayu devushku")
		}
		else if candyNumber == 3 {
			print("Brosayu uchebu")
		}
		else {
			print("Brosayu candy")
		}
		
		switch candyNumber {
		case 1:
			print("Brosayu rabotu")
		case 2:
			print("Brosayu devushku")
		case 3:
			print("Brosayu uchebu")
		default:
			print("Brosayu candy")
		}
		
		print("lucky number is", candyNumber)
		bar()
		foo(c: cokainc)
	}
	
	func foo(c: Int) {
		print("c =", c)
	}
	
	func bar() {
		let a = 6 + Int.random(in: 1...2)
		
		if a < 3 {
			let b = 11
			print(a + b + a + +b + a)
		}
		let b = 3
		print(a + b)
	}
	
	func unaryOperators() {
		//arithmetic
		let a = 5
		let b = -a
		print(b)
		
		let c = +b
		print(c)
		//		+ - * / %
		var sum = (a + b) * c
		print(sum)
		
		sum = sum - 200
		sum -= 200
		
		//logic
		let age = 16
		// < > <= >= == !=
		let ageIsAdoult = age > 18
		let money = Int.random(in: 500000...2000000)
		let pocketFullOfMoney = money > 1000000
		print("pocketFullOfMoney", pocketFullOfMoney)
//		if !ageIsAdoult {
//			print("school")
//		}
//		else {
//			print("private casino")
//		}
		
		// 1				1
		// 0				1
		// 1				0
		// 0				0
		if ageIsAdoult && pocketFullOfMoney {
			print("private casino")
		}
		else {
			print("school")
		}
		
		
	}

}

