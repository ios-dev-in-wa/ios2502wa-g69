

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var textField: UITextField!
	var objects = [String]()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		textField.delegate = self
		
		objects = """
//
//  ViewController.swift
//  G69L11
//
//  Created by Ivan Vasilevich on 4/8/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//
""".components(separatedBy: "\n")
		
		tableView.dataSource = self
		tableView.delegate = self
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		view.endEditing(true)
	}
	
	@IBAction func buttonPressed(_ sender: UIButton) {
		print("buttonPressed")
	}
	@IBAction func tapRecognized(_ sender: UITapGestureRecognizer) {
		print("tapRecognized")
		guard let cell = sender.view?.superview?.superview as? UITableViewCell else {
			return
		}
		let indexPath = tableView.indexPath(for: cell)
		print(indexPath)
	}
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		let objectToDisplay = objects[indexPath.row]
//		cell.textLabel?.text = objectToDisplay
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let name = objects[indexPath.row]
		print(name)
		performSegue(withIdentifier: "showUser", sender: name)
	}

}

extension ViewController: UITextFieldDelegate {
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									   green: CGFloat(arc4random_uniform(256))/255,
									   blue: CGFloat(arc4random_uniform(256))/255,
									   alpha: 1)
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		print(textField.text! + string)
		return true
	}
	
}

