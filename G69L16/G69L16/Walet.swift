//
//  Walet.swift
//  G69L16
//
//  Created by Ivan Vasilevich on 4/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class Walet: NSObject {
	
	static let nashChangedNotificationName = NSNotification.Name(rawValue: "nashChangedNotification")
	static let shared = Walet()
	
	var cash: Int = 0
	
	func addCash(amount: Int) {
		cash += amount
		
		NotificationCenter.default.post(name: Walet.nashChangedNotificationName,
										object: self,
										userInfo: ["kNewCashAmount": cash])
	}

}
