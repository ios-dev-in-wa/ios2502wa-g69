//
//  SecondViewController.swift
//  G69L16
//
//  Created by Ivan Vasilevich on 4/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import SDWebImage

class SecondViewController: UIViewController {
	
	@IBOutlet weak var imageBox: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}


	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		Walet.shared.addCash(amount: 100)
		let imageUrl = URL(string: "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2019/04/22193733/2019-Formula-Drift-Long-Beach-Show-Cars_Trevor-Ryan-Speedhunters_027_1244.jpg")
		imageBox.sd_setImage(with: imageUrl, completed: nil)
	}
	
}

