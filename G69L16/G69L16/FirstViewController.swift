//
//  FirstViewController.swift
//  G69L16
//
//  Created by Ivan Vasilevich on 4/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

	@IBOutlet weak var cashLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.\
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(updateCashLabel(notification:)),
											   name: Walet.nashChangedNotificationName,
											   object: nil)
		Walet.shared.addCash(amount: 0)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
//		performSegue(withIdentifier: "showTutorial", sender: nil)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		Walet.shared.addCash(amount: 100)
	}

	@objc func updateCashLabel(notification: Notification) {
		guard
			let userInfo = notification.userInfo,
			let cash = userInfo["kNewCashAmount"] as? Int
			else { return }
		
		cashLabel.text = cash.description
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}

}

