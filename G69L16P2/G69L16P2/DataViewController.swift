//
//  DataViewController.swift
//  G69L16P2
//
//  Created by Ivan Vasilevich on 4/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var dataLabel: UILabel!
	var dataObject: String = ""


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.dataLabel!.text = dataObject
		imageView.image = UIImage(named: dataObject)
	}


}

