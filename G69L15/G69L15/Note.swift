//
//  Note.swift
//  G69L15
//
//  Created by Ivan Vasilevich on 4/22/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

struct NotesResponse: Codable {
	var results: [Note]?
}

struct Note: Codable {
	
	var objectId: String?
	var text: String?

}
