//
//  APIManager.swift
//  G69L15
//
//  Created by Ivan Vasilevich on 4/22/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class APIManager: NSObject {
	
	static func foo(XY: Int = 0, z: Int = 50) {
		print(XY * 5)
	}
	

	static func getNotes(completion: (([Note]) -> Void)? = nil ) {
		foo(XY: 1)
		foo(XY: 22)
		foo()
		/* Configure session, choose between:
		* defaultSessionConfiguration
		* ephemeralSessionConfiguration
		* backgroundSessionConfigurationWithIdentifier:
		And set session-wide properties, such as: HTTPAdditionalHeaders,
		HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
		*/
		let sessionConfig = URLSessionConfiguration.default
		
		/* Create session, and optionally set a URLSessionDelegate. */
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
		
		/* Create the Request:
		Notes (GET https://parseapi.back4app.com/classes/Note/)
		*/
		
		guard let URL = URL(string: "https://parseapi.back4app.com/classes/Note/") else {return}
		var request = URLRequest(url: URL)
		request.httpMethod = "GET"
		
		// Headers
		
		request.addValue("VQgf2SVxwgncpYDJb8w7GsAfrGOUwMpEpgGP7veN", forHTTPHeaderField: "X-Parse-REST-API-Key")
		request.addValue("yPsEqFKPSAtPDAH5UlFYcMsj7W17GsoVTapwaQOE", forHTTPHeaderField: "X-Parse-Application-Id")
		
		/* Start a new Task */
		let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			if (error == nil) {
				// Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				
				
				let decoder = JSONDecoder()
				
				do {
					let notesResponse = try decoder.decode(NotesResponse.self, from: data!)
					completion?(notesResponse.results!)
				} catch {
					print(error.localizedDescription)
				}
			}
			else {
				// Failure
				print("URL Session Task Failed: %@", error!.localizedDescription);
			}
		})
		task.resume()
		session.finishTasksAndInvalidate()
	}
	
	static func saveNote(_ note: Note, completion: ((Bool) -> Void)? = nil) {
		/* Configure session, choose between:
		* defaultSessionConfiguration
		* ephemeralSessionConfiguration
		* backgroundSessionConfigurationWithIdentifier:
		And set session-wide properties, such as: HTTPAdditionalHeaders,
		HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
		*/
		let sessionConfig = URLSessionConfiguration.default
		
		/* Create session, and optionally set a URLSessionDelegate. */
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
		
		/* Create the Request:
		CreateNote (POST https://parseapi.back4app.com/classes/Note/)
		*/
		
		guard let URL = URL(string: "https://parseapi.back4app.com/classes/Note/") else {return}
		var request = URLRequest(url: URL)
		request.httpMethod = "POST"
		
		// Headers
		
		request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
		request.addValue("VQgf2SVxwgncpYDJb8w7GsAfrGOUwMpEpgGP7veN", forHTTPHeaderField: "X-Parse-REST-API-Key")
		request.addValue("yPsEqFKPSAtPDAH5UlFYcMsj7W17GsoVTapwaQOE", forHTTPHeaderField: "X-Parse-Application-Id")
		
		// JSON Body
		
		let bodyObject: [String : Any] = [
			"text": note.text ?? ""
		]
		request.httpBody = try! JSONSerialization.data(withJSONObject: bodyObject, options: [])
		
		/* Start a new Task */
		let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			if (error == nil) {
				// Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				print("URL Session Task Succeeded: HTTP \(statusCode)")
				completion?(true)
			}
			else {
				// Failure
				print("URL Session Task Failed: %@", error!.localizedDescription);
				completion?(false)
			}
		})
		task.resume()
		session.finishTasksAndInvalidate()
	}
}




