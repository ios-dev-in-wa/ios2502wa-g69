//
//  ViewController.swift
//  G69L4
//
//  Created  by Ivan Vasilevich on 3/11/19.
//  Copyright © 2019  RockSoft. All rights  reserved.
//

import UIKit

class ViewController: UIViewController {

	var n = 5.0
	var square = 0.0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
//		squareOfNumber(number: n)
//		n = 3
//		squareOfNumber()
//		let sqOf3 = square
//
//		n = 4
//		squareOfNumber()
//		square = 100500
//		let sqOf4 = square
		
//		print("sqOf3 + sqOf4 = ", sqOf3 + sqOf4)
		
//		print("sqOf3 + sqOf4 = ", squareOfNumber(3) + squareOfNumber(4))
		
		workWithStrings()
//		createInstanceOfDifferentClasses()
		workWithNSStrings()
	}
	
	func newstedLoops() {
		for i in 0..<5 {
			for j in 0..<5 {
				let ref = i * 5 + j
				if ref % 3 == 0 {
					print("Hello i = \(i) \t j = \(j)\t ref", ref)
				}
			}
		}
		
		
		var age = 25
		
		while age < 65 {
			print("work hard", age)
			age += 1
		}
		print("pencii")
	}
	
	func squareOfNumber(_ number: Double) -> Double {
		let result = number * number
		print("squareOfNumber \(number) = \(result)")
		return result
	}
	
	func squareOfNumber() {
		let result = n * n
		square = result
		print("squareOfNumber \(n) = \(result)")
	}
	
	func workWithStrings() {
		let name = "Ivan"
		let surname = "Besarab"
		let fullName =  name + " " + surname
		
		print(fullName)
		
		let longStr = """
//
//  ViewController.swift
//  G69L4
//
//  Created  by Ivan Vasilevich on 3/11/19.
//  Copyright © 2019  RockSoft. All rights  reserved.
//
"""
		print(longStr)
		print(longStr.count)
		print("------------------------------------------------------")
		print(longStr.replacingOccurrences(of: "  ", with: "*").count)
		
		let number = 134
		
		let max = String(number)
	}
	
	func workWithNSStrings() {
		let name = ("Iv256an" as NSString) as String
		let name2 = "ar777kan" as NSString
		let range = NSRange.init(location: 2, length: 3)
		let numbers = name2.substring(with: range)
		
		print(numbers)
		
		
	}
	
	func createInstanceOfDifferentClasses() {
//		Int
//		let someInt = Int.init()
		let someInt = Int() // 0
//		String
		let someStr = String() // ""
//		Date
		let someDate = Date() // current date
		print(someDate)
	}


}

