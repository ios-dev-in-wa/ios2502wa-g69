//
//  MasterViewController.swift
//  G69L14
//
//  Created by Ivan Vasilevich on 4/19/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	
	var objects = [PFObject]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
		
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		
		reloadDataFromParse()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if PFUser.current() == nil {
			showLoginVC()
		}
	}

	@objc
	func insertNewObject(_ sender: UIBarButtonItem) {
		showAlertToAddNote()
//		objects.insert(NSDate(), at: 0)
//		let indexPath = IndexPath(row: 0, section: 0)
//		tableView.insertRows(at: [indexPath], with: .automatic)
	}
	
	func showAlertToAddNote() -> Void {
		let alertConreoller = UIAlertController(title: "Enter new note",
												message: nil,
												preferredStyle: .alert)
		
		alertConreoller.addTextField { (textField) in
			textField.placeholder = "Yazzz 4000"
		}
		
		let cancelAction: ((UIAlertAction) -> Void)? = nil
		
		alertConreoller.addAction(UIAlertAction(title: "Cancel",
												style: .cancel,
												handler: cancelAction))
		
		alertConreoller.addAction(UIAlertAction(title: "Save",
												style: .default,
												handler: { (act) in
													print("Save pressed")
													self.saveNoteWith(text: (alertConreoller.textFields?.first?.text ?? "no text"))
													
		}))
		
		present(alertConreoller, animated: true)
		
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row]
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row]
		cell.textLabel!.text = object["text"] as? String
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}
	
	private func saveNoteWith(text: String) {
		let note = PFObject(className: "Note")
		note["text"] = text
		if let currentUser = PFUser.current() {
			note["owner"] = currentUser
		}
		note.saveInBackground { (success, error) in
			if success {
				self.reloadDataFromParse()
				PFUser.current()?.relation(forKey: "notes").add(note)
				PFUser.current()?.saveInBackground(block: { (success, error) in
					print(success, error?.localizedDescription ?? "no error")
				})
			}
			else {
				print(error!.localizedDescription)
			}
			
		}
	}
	
	private func reloadDataFromParse() {
		let query = PFQuery(className: "Note")
		
		query.order(byDescending: "createdAt")
		
		if let currentUser = PFUser.current() {
			query.whereKey("owner", equalTo: currentUser)
		}
		
		
		query.findObjectsInBackground { (optionalObjects, error) in
			if let realObjects = optionalObjects {
				self.objects = realObjects
				self.tableView.reloadData()
			}
			else {
				print(error!)
			}
			
			self.refreshControl?.endRefreshing()
			
		}
	}
	
	private func showLoginVC() {
		let loginVC = PFLogInViewController()
		present(loginVC, animated: true, completion: nil)
	}

	@IBAction func refreshActivated(_ sender: UIRefreshControl) {
		reloadDataFromParse()
	}
	
}

