//
//  DetailViewController.swift
//  G69L14
//
//  Created by Ivan Vasilevich on 4/19/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!


	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail["text"] as? String
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		configureView()
	}

	var detailItem: PFObject? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}


}

