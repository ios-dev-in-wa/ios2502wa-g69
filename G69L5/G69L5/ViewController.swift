//
//  ViewController.swift
//  G69L5
//
//  Created by Ivan Vasilevich on 3/15/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	let car2 = Car.init()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		charSetCheck()
//		numberFromString("023")
		
		arrays()
//		dictionary()
//		drawBox()
		workWithCars()
	}
	
	func charSetCheck() {
		let charSet = CharacterSet.decimalDigits
		print(charSet)
		
		let str = "qwer5tyuio"
		
		if str.rangeOfCharacter(from: charSet) != nil {
			print("yes")
		}
		else {
			print("no")
		}
	}
	
	func numberFromString(_ str: String) {
		let number = Int(str)
		
		
		if let realNumber = number { // optional binding
			print(realNumber)
		}
		else {
			print("enter correct number")
		}
		
		print((number ?? 10) + 5) // default value
		
		print(number!) //force unwrap
		
		
	}
	
	func arrays() {
		let numbers = [5, 3, 18]
		
//		print(numbers[0])
		
		for i in 0..<numbers.count {
			let element = numbers[i]
			print(element)
		}
		
		var elements = [12, 25]
		print(elements)
		
		elements.insert(222, at: 1)
		
		print(elements)
		
		print(elements.index(of: 215) ?? -1)
		
		elements.removeAll()
		print(elements)
		
		
		for _ in 0..<20 {
			elements.append(Int.random(in: 0...10))
		}
		
		print(elements)
		
	}
	
	func dictionary() {
		var phoneBook = ["FireFighters" : "101",
						 "Ambulance" : "103",
						 "Police" : "102",
						 "Gas" : "104"]
		
		print(phoneBook["AmbDSulance"])
		
		phoneBook["Gas"] = "12345"
		phoneBook.removeValue(forKey: "Ambulance")
		
		for kv in phoneBook {
			print("key: \(kv.key) = \(kv.value)")
		}
	}

	func drawBox() {
		let box = UIView.init(frame: CGRect(x: 25, y: 50, width: 32, height: 32))
		box.backgroundColor = UIColor.blue
		view.addSubview(box)
	}
	
	func workWithCars() {
		
		car2.color = "red"
		
		car2.addFuel(amount: 20)
		print(car2.description)
	}
}

