//
//  Car.swift
//  G69L5
//
//  Created by Ivan Vasilevich on 3/15/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

enum Drink: String {
	case water = "WATER"
	case coffe = "COFFE"
}

func printDrinkName(_ drink: Drink) {
	print("yuu choose: \"\(drink.rawValue)\"")
}

class Car: NSObject {
	
	private var fuel = 0
	private let maxFuel = 80
	
	override var description: String {
		return "car doorsCount: \(doorsCount), color: \(color)"
	}
	
	var doorsCount: Int = 2
	var color = "gray"
	
	func addFuel(amount: Int) {
		printDrinkName(.water)
		if fuel + amount < maxFuel {
			fuel += amount
		}
		else {
			print("too much fuel amount")
		}
	}

}
