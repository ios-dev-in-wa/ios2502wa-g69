//
//  ViewController.swift
//  G69L10
//
//  Created by Ivan Vasilevich on 4/5/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

struct Car {
	var weight = 1000
	var speed = 10
}

class Vehicle {
	var weight = 1000
	var speed = 10
}

class ViewController: UIViewController {
	
	let face = FaceView.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		let arrInt = [1, 2]
		var arrStr = arrInt.map { (number) -> String in
			return "#\(number)"
		}
		
		arrStr = "abcd".map({ (char) -> String in
			return char.description.uppercased()
		})
		
		arrStr.append("da")
		
		print(arrStr)
		
		var car1 = Car()
		car1.speed += 4
//		chancgeCar(car1)
		print(car1.speed)
		var car2 = car1
		car2.speed += 100
		print(car2.speed)
		print(car1.speed)
		
		let vehicle1 = Vehicle()
		car1.speed += 4
		chancgeCar(vehicle1)
		print(vehicle1.speed)
		let vehicle2 = vehicle1
		vehicle2.speed += 100
		print(vehicle2.speed)
		print(vehicle1.speed)
		
		
		addFace()
		addGesture()
	}
	
	func addGesture() {
		let tap = UITapGestureRecognizer(target: self, action: #selector(moveView(_:)))
		view.addGestureRecognizer(tap)
	}
	
	@objc func moveView(_ sender: UITapGestureRecognizer) {
		let position = sender.location(in: view)
		face.center = position
	}
	
	func chancgeCar(_ car: Vehicle) {
		car.speed += 5
	}
	
	func addFace() {
		
		face.changeHappyFace(withHappLevel: 40, andColor: .red)
		view.addSubview(face)
	}
	
	@IBAction func scaleView(_ sender: UIPinchGestureRecognizer) {
//		print(sender.scale)
		sender.view?.frame.size.width *= sender.scale
		sender.scale = 1
		
		print(sender.location(in: view))
	}


}

