//
//  ViewController.swift
//  G69L7
//
//  Created by Ivan Vasilevich on 3/22/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var textView: UITextView!
	
	let kRunCount = "runCount"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let color = UIColor.init(hexString: "#1b62d3")
		view.backgroundColor = color
		displayImage()
		displayText()
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		var count = UserDefaults.standard.integer(forKey: kRunCount)
		count += 1
		UserDefaults.standard.set(count, forKey: kRunCount)
		print(count)
	}
	
	func displayImage() {
		let img = UIImage.init(named: "AngryBirds")
		imageView.image = img
	}

	func displayText() {
		let storage = TextStorage()
		let textFromModel = storage.defaultText
		textView.text = textFromModel
		
//		textView.text = TextStorage.init().defaultText
	}

}

