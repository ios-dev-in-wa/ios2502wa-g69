//
//  VC2.swift
//  G69L9
//
//  Created by Ivan Vasilevich on 4/1/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class VC2: UIViewController {
	
	@IBOutlet weak var label: UILabel!
	
	var stringToDisplay = String() //""

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .red
		label.text = stringToDisplay
    }
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		if let prevVC = navigationController?.viewControllers.first as? ViewController1 {
			prevVC.messageFromNextVC = Date().description
		}
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		dismiss(animated: true, completion: nil)
	}

}
