//
//  VC3.swift
//  G69L9
//
//  Created by Ivan Vasilevich on 4/1/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class VC3: VC2 {
	
	var intToDisplay = 0

    override func viewDidLoad() {
        super.viewDidLoad()

       view.backgroundColor = .green
		
		label.text = intToDisplay.description
    }
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		
		if let prevVCNav = presentingViewController as? UINavigationController {
			if let prevVC = prevVCNav.viewControllers.first as? ViewController1 {
				prevVC.messageFromNextVC = Date().description
			}
			
		}
		
	}
    


}
