//
//  ViewController.swift
//  G69L9
//
//  Created by Ivan Vasilevich on 4/1/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

	@IBOutlet weak var textField: UITextField!
	
	var messageFromNextVC = "KROKODIL"
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationItem.title = messageFromNextVC
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc2 = segue.destination as? VC2 {
			if let senderAsString = sender as? String {
				vc2.stringToDisplay = senderAsString
			}
		}
		
		if let vc3 = segue.destination as? VC3 {
			vc3.intToDisplay = 55
		}
	}
	
	@IBAction func showText(_ sender: UISegmentedControl) {
		let selectedIndex = sender.selectedSegmentIndex
		let prefix = sender.titleForSegment(at: selectedIndex)!
		
		let wordToSend = prefix + " " + textField.text!
		
		//		print(wordToSend)
		
		performSegue(withIdentifier: "showText\(selectedIndex+2)", sender: wordToSend)
		
	}
}

