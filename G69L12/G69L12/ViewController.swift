//
//  ViewController.swift
//  G69L12
//
//  Created by Ivan Vasilevich on 4/12/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	
	@IBOutlet weak var box: UIView!
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch = touches.first!
		let pos = touch.location(in: view)
		
		
//		UIView.animate(withDuration: 0.3) {
//			self.box.center = pos
//		}
		
		func foo() {
			box.center = pos
			box.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
										  green: CGFloat(arc4random_uniform(256))/255,
										  blue: CGFloat(arc4random_uniform(256))/255,
										  alpha: 1)
			box.transform = CGAffineTransform.init(rotationAngle: .pi)
		}
		
//		UIView.animate(withDuration: 0.3,
//					   animations: {
//			foo()
//		}) { (finished) in
//			if finished {
//				print("alloha")
//			}
//		}
		
		UIView.animate(withDuration: 0.4, delay: 1,
					   usingSpringWithDamping: 0.5,
					   initialSpringVelocity: 0.3, options: [], animations: {
			foo()
		}, completion: nil)
		
//		UIView.transition(with: view, duration: 1, options: .transitionCrossDissolve, animations: {
//			self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
//										   green: CGFloat(arc4random_uniform(256))/255,
//										   blue: CGFloat(arc4random_uniform(256))/255,
//										   alpha: 1)
//		}) { (_) in
//			print("animation complete")
//		}
	}


}

