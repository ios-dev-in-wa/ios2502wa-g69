//
//  RedVC.swift
//  G69L8
//
//  Created by Ivan Vasilevich on 3/25/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class RedVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
		view.backgroundColor = .orange
        // Do any additional setup after loading the view.
    }
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		navigationController?.popViewController(animated: true)
	}

}
