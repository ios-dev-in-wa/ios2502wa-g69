//
//  SokPresentorVC.swift
//  G69L8
//
//  Created by Ivan Vasilevich on 3/25/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class SokPresentorVC: UIViewController {

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		performSegue(withIdentifier: "showSok", sender: nil)
	}

}
