//
//  ViewController.swift
//  G69L6
//
//  Created by Ivan Vasilevich on 3/18/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var centerLabel: UILabel!

	@IBAction func buttonPressed() {
		centerLabel.text = "buttonPressed"
		view.backgroundColor = UIColor.init(red: CGFloat(arc4random_uniform(256))/255,
											green: CGFloat(arc4random_uniform(256))/255,
											blue: CGFloat(arc4random_uniform(256))/255,
											alpha: 1)
	}
	
	@IBAction func setLabelTextColor(_ sender: UIButton) {
		let color = [UIColor]([.blue, .yellow, .green])[sender.tag]
		centerLabel.textColor = color
	}
	
}

