//
//  ViewController.swift
//  G69L13
//
//  Created by Ivan Vasilevich on 4/15/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
	
	var mafon: AVAudioPlayer!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.

	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		showAlert()
	}
	
	func handleCancel() {
		print("cancell pressed")
		print("handleCancel")

		playSound()
	}
	
	func playSound() {
		
		guard let filePath = Bundle.main.url(forResource: "Valesco - All I Need", withExtension: "mp3")  else {
			return
		}
		mafon = try? AVAudioPlayer(contentsOf: filePath)
		mafon.play()
	}
	
	func showAlert() -> Void {
		print("foo")
		let alertConreoller = UIAlertController(title: "Foo!",
												message: "Foo?",
												preferredStyle: .alert)
		
		alertConreoller.addTextField { (textField) in
			textField.placeholder = "Yazzz4000"
		}
		
		let cancelAction: ((UIAlertAction) -> Void)? = ({ (act) in
			self.handleCancel()
		})
		
		alertConreoller.addAction(UIAlertAction(title: "foo()1",
												style: .cancel,
												handler: cancelAction))
		
		alertConreoller.addAction(UIAlertAction(title: "<foo>2",
												style: .default,
												handler: { (act) in
													print("Accept pressed")
													print(alertConreoller.textFields?.first?.text ?? "no text")
													
		}))
		
		
		
		present(alertConreoller,
				animated: true,
				completion: {
					self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
														green: CGFloat(arc4random_uniform(256))/255,
														blue: CGFloat(arc4random_uniform(256))/255,
														alpha: 1)
		})
		
		
	}

}

